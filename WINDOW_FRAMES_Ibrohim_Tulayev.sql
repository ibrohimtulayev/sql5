WITH SalesData AS (
    -- Assuming your table is named 'sales_table', replace it with your actual table name
    SELECT
        time_id,
        sales,
        ROW_NUMBER() OVER (ORDER BY time_id) as row_num
    FROM
        sales_table
    WHERE
        EXTRACT(WEEK FROM time_id) IN (49, 50, 51)
)

SELECT
    time_id,
    ROW_NUMBER() OVER (ORDER BY time_id) as week_number,
    EXTRACT(DAY FROM time_id) as day_of_week,
    sales,
    SUM(sales) OVER (ORDER BY time_id) as cum_sum,
    AVG(sales) OVER (
        ORDER BY time_id
        RANGE BETWEEN 2 PRECEDING AND 2 FOLLOWING
    ) as centered_3_day_avg
FROM
    SalesData
ORDER BY
    time_id;
